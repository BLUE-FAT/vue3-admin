import { createRouter as Router, createWebHashHistory } from "vue-router";

import Layout from "@/components/layout/smart-layout/index.vue";

// import Layout from "@/components/layout/smart-layout-vertical/index.vue";

/**
 *
 * hidden: true                   在侧边栏隐藏
 * alwaysShow: true               在侧边栏显示
 * redirect: noRedirect           面包屑中不添加链接
 * name:'router-name'             用于 <keep-alive> 
 * meta : {
    roles: ['admin','editor']     异步路由的权限
    title: 'title'                侧边栏和面包屑的名字
    icon: 'svg-name'              侧边栏图标
    noCache: true                 不缓存页面
    affix: true                   在tab栏中常驻
    breadcrumb: false             不显示在面包屑
    activeMenu: '/example/list'   侧边栏选项高亮
  }
 */

export const constantRoutes = [
  {
    path: "/redirect",
    component: Layout,
    hidden: true,
    children: [
      {
        path: "/redirect/:path(.*)*",
        component: () => import("@/views/redirect/index.vue")
      }
    ]
  },
  {
    path: "/login",
    component: () => import("@/views/Login.vue"),
    hidden: true
  },
  {
    path: "/",
    component: Layout,
    redirect: "/homepage",
    children: [
      {
        path: "homepage",
        name: "homepage",
        component: () => import("@/views/Homepage.vue"),
        meta: { title: "首页", icon: "el-icon-s-flag" }
      }
    ]
  },

  {
    path: "/:pathMatch(.*)*",
    redirect: "/"
  }
];

export const asyncRoutes = [];

const createRouter = () =>
  Router({
    history: createWebHashHistory(),
    routes: constantRoutes,
    scrollBehavior: () => ({ left: 0, top: 0 })
  });

const router = createRouter();

export function resetRouter() {
  const newRouter = createRouter();
  router.matcher = newRouter.matcher; // reset router
}

export default router;
