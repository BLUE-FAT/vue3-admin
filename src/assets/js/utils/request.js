import { ElMessage } from "element-plus";
import axios from "axios";
import md5 from "js-md5";
import { getRefreshToken } from "@/api/user";
import store from "@/store";

const service = axios.create({
  baseURL: import.meta.env.VITE_APP_BASE_API, // url = base url + request url
  timeout: 50000 // request timeout
});

service.interceptors.request.use(
  config => {
    if (store.getters.token) {
      // token续期，判断token是否需要续期
      const nowTime = new Date().valueOf(); // 当前时间戳
      const timestamp = sessionStorage.getItem("timestamp");
      const leastTime = nowTime - timestamp; // token使用时间
      const refreshTokenTime = 5; // 自动刷新token天数
      const tokenExpirationDate = 7; // 服务器token有效天数
      // 如果大于5天就重新刷新token，然后存进去
      if (leastTime > 1000 * 60 * 60 * 24 * refreshTokenTime && leastTime < 1000 * 60 * 60 * 24 * tokenExpirationDate) {
        const refreshToken = sessionStorage.getItem("refreshToken");
        getRefreshToken({ refreshToken })
          .then(res => {
            // 刷新了token以后重新存入
            store.commit("user/SET_TOKEN", res.data.token);
            sessionStorage.setItem("token", res.data.token);
            sessionStorage.setItem("refreshToken", res.data.refreshToken);
            sessionStorage.setItem("timestamp", res.data.timestamp);
          })
          .catch(e => {
            console.log(`续期token失败 ${e.response.data.message || e.response.data.msg || e}`);
          });
      }
      config.headers.Authorization = store.getters.token;
      // 方法签名，用于判断重复点击，不要删除
      const params = !config.params ? "" : JSON.stringify(config.params);
      const data = !config.data ? "" : JSON.stringify(config.data);
      const source = `${config.method}-${config.url}${data || params}${store.getters.token}`;
      config.headers.sign = md5(source);
    }
    return config;
  },
  error => {
    console.log(error); // for debug
    return Promise.reject(error);
  }
);

service.interceptors.response.use(
  response => {
    const res = response.data;
    if (res.status !== 1) {
      // token失效情况
      switch (res.status) {
        case 401:
          ElMessage({
            message: "登录已过期,请重新登录",
            type: "warning",
            duration: 5 * 1000
          });
          store.dispatch("user/resetToken").then(() => {
            this.$router.push(`/login?redirect=${this.$route.fullPath}`);
          });
          break;
        default:
          ElMessage({
            message: res.message || res.msg || "Error",
            type: "error",
            duration: 5 * 1000
          });
      }
      return Promise.reject(res.message || res.msg || "Error");
    }
    if (res.msg === "repeat") {
      console.log("重复操作，忽略！");
      return Promise.reject();
    }
    return res;
  },
  error => {
    console.log(error.response); // for debug
    ElMessage({
      message: error.response.data.msg || error.response.message || error,
      type: "error",
      duration: 5 * 1000
    });
    return Promise.reject(error);
  }
);

export default service;
