import { createStore } from "vuex";
import getters from "./getters";

const modulesFiles = import.meta.globEager("./modules/*.js");

const modules = Object.keys(modulesFiles).reduce((module, modulePath) => {
  const moduleName = modulePath.replace(/^\.\/modules\/(.*)\.\w+$/, "$1");
  const value = modulesFiles[modulePath];
  module[moduleName] = value.default;
  return module;
}, {});

const store = createStore({
  modules,
  getters
});

export default store;
