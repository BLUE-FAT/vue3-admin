import defaultSettings from "@/assets/js/settings";

const { tagsView, fixedHeader, sidebarLogo } = defaultSettings;

const state = {
  tagsView,
  fixedHeader,
  sidebarLogo
};

const mutations = {
  CHANGE_SETTING: (state, { key, value }) => {
    if (state.hasOwnProperty(key)) {
      state[key] = value;
    }
  }
};

const actions = {
  changeSetting({ commit }, data) {
    commit("CHANGE_SETTING", data);
  }
};

export default {
  namespaced: true,
  state,
  mutations,
  actions
};
