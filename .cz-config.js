module.exports = {
  // type 类型（定义之后，可通过上下键选择）
  types: [
    { value: "feat", name: "feat:    增加功能" },
    { value: "fix", name: "fix:     修复bug" },
    { value: "style", name: "style:   样式修改" },
    { value: "refactor", name: "refactor: 代码重构" },
    { value: "docs", name: "docs:     修改文档" },
    { value: "init", name: "init:    初始提交" },
    { value: "chore", name: "chore:    依赖变更" },
    { value: "revert", name: "revert:   代码回退" },
    { value: "build", name: "build:   更改配置" },
    { value: "del", name: "del:   删除代码" }
  ],

  // scope 类型（定义之后，可通过上下键选择）
  scopes: [
    ["component", "组件相关"],
    ["util", "公共相关"],
    ["style", "样式相关"],
    ["deps", "项目依赖"],
    ["other", "其他修改"]
    // 如果选择 custom，后面会让你再输入一个自定义的 scope。也可以不设置此项，把后面的 allowCustomScopes 设置为 true
    // ["custom", "以上都不是？我要自定义"]
  ].map(([value, description]) => {
    return {
      value,
      name: `${value.padEnd(30)} (${description})`
    };
  }),

  // 是否允许自定义填写 scope，在 scope 选择的时候，会有 empty 和 custom 可以选择。
  // allowCustomScopes: true,

  // allowTicketNumber: false,
  // isTicketNumberRequired: false,
  // ticketNumberPrefix: 'TICKET-',
  // ticketNumberRegExp: '\\d{1,5}',

  // 针对每一个 type 去定义对应的 scopes，例如 fix
  /*
  scopeOverrides: {
    fix: [
      { name: 'merge' },
      { name: 'style' },
      { name: 'e2eTest' },
      { name: 'unitTest' }
    ]
  },
  */

  // 交互提示信息
  messages: {
    type: "选择更改类型:",
    scope: "更改的范围:",
    // 选择 scope: custom 时会出下面的提示
    customScope: "请输入自定义的scope:",
    subject: "简短描述:",
    body: "详细描述:",
    breaking: "非兼容性重大变更:",
    footer: "关闭的issues列表:",
    confirmCommit: "确认提交?"
  },

  // 设置只有 type 选择了 feat 或 fix，才询问 breaking message
  allowBreakingChanges: [],

  // 跳过要询问的步骤
  skipQuestions: ["body", "footer"],

  subjectLimit: 100, // subject 限制长度
  breaklineChar: "|" // 换行符，支持 body 和 footer
  // footerPrefix : 'ISSUES CLOSED:'
  // askForBreakingChangeFirst : true,
};
